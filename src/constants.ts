export const HTTP_STATUS = {
  NOT_AUTHORIZED: 401,
  OK: 200,
  BAD_REQUEST: 400,
  NOT_FOUND: 404,
  CREATED: 201,
  FORBIDDEN: 403,
  CONFLICT: 409,
};

export const JWT_SECRET = `agrainess@#1app`;