const error = (text: string) => {
  console.log('ERROR:', text);
};

const info = (text: string) => {
  console.log('INFO:', text);
};

export default {
  error,
  info,
};
