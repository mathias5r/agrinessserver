import mongodb from '../database/mongo';

export const getCollection = async (name: String) => {
  const db = await mongodb.agrinessDB;
  return db.collection(name);
};
