import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import { JWT_SECRET } from '../constants';

export const generateSalt = () =>
  crypto
    .randomBytes(Math.ceil(16 / 2))
    .toString(`hex`)
    .slice(0, 16);

const generatePasswordHash = (password: string, salt: string) => {
  return crypto
    .createHmac(`sha256`, salt)
    .update(password)
    .digest(`hex`);
};

const generateToken = (email: string) => {
  const token = jwt.sign({email}, JWT_SECRET);
  return token;
};

const checkCredentials = (password: string, hash: string, salt: string) => {
  const passwordHash = generatePasswordHash(password, salt);
  return passwordHash === hash;
}

const verifyToken = (authorization: string) => jwt.verify(authorization, JWT_SECRET);

export default { 
  checkCredentials,
  generateToken,
  generateSalt,
  generatePasswordHash,
  verifyToken
}
