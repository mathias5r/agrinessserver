interface FaseProducao {
  sigla: string,
  producao: string,
}

interface TipoGranja {
  sigla: string,
  producao: string,
}

export interface Animal {
  id: string,
  nome: string,
  tipoAnimal: string,
  localizacao: string,
  dataNascimento: string,
  entradaPlantel: string,
  pesoCompra: string,
  raca: string,
  codigoRastreamento: string,
  faseProducao: FaseProducao,
  tipoGranja: TipoGranja,
}