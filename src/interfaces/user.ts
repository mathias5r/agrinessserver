export interface User {
  email: string, 
  password: string,
  salt: string,
}