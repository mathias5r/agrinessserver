import * as express from 'express'
import animals from '../models/animals';

const animalsController = express.Router({ mergeParams: true });

animalsController.get(`/`, async (req: express.Request, res: express.Response)=>{
  const params = req.query;
  const limit = Number(params.limit) && typeof Number(params.limit) === "number" ? Number(params.limit) : 0;
  const skip = Number(params.skip) && typeof Number(params.skip) === "number" ? Number(params.skip) : 0;
  try {
    const list = await animals.getAll(limit,skip);
    res.send(list);
  } catch (e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `})
  }
});

animalsController.get(`/:id`,async  (req: express.Request, res: express.Response)=>{
  try {
    const id = req.params.id;
    const animal = await animals.getByID(id)
    if(!animal){
      return res.status(404).send(`Animal not found`)
    }
    res.send(animal)
  } catch(e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `})
  }
});

animalsController.post(`/`,async  (req: express.Request, res: express.Response)=>{
  try {
    const animal = req.body;
    const id = await animals.create(animal)
    res.status(200).send({ id: id })
  } catch(e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `})
  }
});

animalsController.patch(`/:id`, async (req: express.Request, res: express.Response)=>{
  try { 
    const id = req.params.id;
    const animal = req.body;
    await animals.update(id, animal)
    res.status(200).send(`Sucessfully updated animal with id: ${id}`);
  } catch(e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `});
  }
});

animalsController.delete(`/:id`, async (req: express.Request, res: express.Response)=>{
  try {
    const id = req.params.id;
    await animals.remove(id);
    res.status(200).send(`Sucessfully deleted animal with id: ${id}`);
  }catch(e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `})
  }
});

export default animalsController;