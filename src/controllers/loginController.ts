import * as express from 'express'
import users from '../models/users';
import { HTTP_STATUS } from '../constants'
import authService from '../services/authService'

const loginController = express.Router({ mergeParams: true });

loginController.post(`/`, async (req: express.Request, res: express.Response)=>{
  const { email, password } = req.body;
  try{
    const user = await users.get(email);
    if(!user || !user.salt || !user.hash){
      return res.status(HTTP_STATUS.NOT_FOUND).send(`User not found`)
    }
    const { hash, salt } = user;  
    if(!authService.checkCredentials(password, hash, salt)){
      return res.status(HTTP_STATUS.NOT_AUTHORIZED).send(`User not authorized`)
    }
    const token = authService.generateToken(email);
    return res.status(200).send(token)
  } catch(e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `});
  }
});

export default loginController;