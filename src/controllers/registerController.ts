import * as express from 'express'
import users from '../models/users'
import authSession from '../services/authService';
import { HTTP_STATUS } from '../constants'

const registerController = express.Router({ mergeParams: true });

registerController.post(`/`, async (req: express.Request, res: express.Response)=> {
  try{
    const { name, email, password } = req.body;
    const user = await users.get(email);
    if(user && user.name){
      return res.status(HTTP_STATUS.CONFLICT).send(`User already exists!`);
    }
    const salt = authSession.generateSalt();
    const hash = authSession.generatePasswordHash(password, salt);
    await users.create(name, email, salt, hash)
    const token = authSession.generateToken(email);
    res.status(HTTP_STATUS.CREATED).send(token);
  } catch(e){
    res.status(400).send({ error: true, message: `[ERROR]: ${e} `});
  }
});

export default registerController;