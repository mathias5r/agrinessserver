import { Animal } from '../interfaces/animal';
import { getCollection } from '../services/mongo'

const collection = 'Animals';

const getAll = async (limit: number, skip: number) => {
  const animalsCollection = await getCollection(collection);
  return animalsCollection.find({}).limit(limit).skip(skip).toArray();
};

const getByID = async (id: String) => {
  const animalsCollection = await getCollection(collection);
  return animalsCollection.findOne({ id });
};

const create = async (animal: Animal ) => {
  const animalsCollection = await getCollection(collection);
  return animalsCollection.insertOne({
    animal
  });
};

const update = async (id: String, animal: Animal) => {
  const animalsCollection = await getCollection(collection);
  return animalsCollection.updateOne({ id }, { $set: animal});
};

const remove = async (id: String) => {
  const animalsCollection = await getCollection(collection);
  return animalsCollection.deleteOne({ id });
}

export default {
  getAll,
  getByID,
  create,
  update,
  remove,
};
