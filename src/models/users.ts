import { User } from '../interfaces/user';
import { getCollection } from '../services/mongo'

const colection = 'Users';

const get = async (email: string) => {
  const usersCollection = await getCollection(colection);
  return usersCollection.findOne({ email });
};

const create = async (name: string, email: string, salt: string, hash: string) => {
  const usersCollection = await getCollection(colection);
  return usersCollection.insertOne({name, email, salt, hash});
};

export default {
  get,
  create,
};
