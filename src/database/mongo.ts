import { MongoClient } from 'mongodb';
import logger from '../services/logger';

interface Config {
  user: string,
  password: string,
  server: string,
  database: string,
}

const getDatabaseConfig = () => ({
  user: process.env.MONGO_USER,
  password: process.env.MONGO_PASSWORD,
  server: process.env.MONGO_HOST,
  database: process.env.MONGO_DATABASE,
});

const client = async () => {
  const { user, password, server, database } = <Config>getDatabaseConfig();
  const connectionString = `mongodb://${user}:${password}@${server}`;
  try {
    const conn = await MongoClient.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true });
    const db = await conn.db(database);
    logger.info(`Sucessfully connected to database!`);
    return db;
  } catch (err) {
    logger.error(`Error trying to connect to database: ${err}`);
    return <any>{};
  }
};

export default {
  agrinessDB: client(),
};
