import * as express from 'express'
import { HTTP_STATUS, JWT_SECRET } from '../constants'
import jwt from 'jsonwebtoken';
import authService from '../services/authService';
import logger from '../services/logger';

export default (req: express.Request , res: express.Response, next: express.NextFunction) => {
  const {
    headers: { authorization },
  } = req;
  if(!authorization){
    return res.sendStatus(HTTP_STATUS.NOT_AUTHORIZED);
  }
  try {
    authService.verifyToken(authorization);
  } catch (err) {
    logger.error(`Unauthorized request | ${err} | Request: ${JSON.stringify(req.body)}`);
    return res.sendStatus(HTTP_STATUS.NOT_AUTHORIZED);
  }
  next();
}