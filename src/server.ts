import express from "express";
import apiRouter, { registerRouter, loginRouter } from './routes';
import bodyParser from "body-parser";
import swaggerUI from 'swagger-ui-express';
import swaggerDocument from '../config/swagger.json'

const server = express();
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.get("/", (_, res) => {
  res.send("Server Up");
});

server.use(`/swagger`, swaggerUI.serve, swaggerUI.setup(swaggerDocument))

server.use("/api/v1", apiRouter)
server.use("/register", registerRouter)
server.use("/login", loginRouter)

export default server;