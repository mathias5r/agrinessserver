import { Router } from 'express';
import authMiddleware from './middlewares/authMiddleware';
import animalsController from './controllers/animalsController';
import loginController from './controllers/loginController';
import registerController from './controllers/registerController';

const apiRouter = Router({ mergeParams: true });
export const loginRouter = Router({ mergeParams: true });
export const registerRouter = Router({ mergeParams: true });

apiRouter.use(`/animals`, authMiddleware, animalsController);
loginRouter.use(`/`, loginController);
registerRouter.use(`/`, registerController)

export default apiRouter;