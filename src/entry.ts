import server from './server';
import logger from './services/logger';

server.listen(8000, () => {
  logger.info(`[SERVER] Running at http://localhost:8000`);
  logger.info(`[SWAGGER] Running at http://localhost:8000/swagger`);
});
