FROM node:10-alpine

WORKDIR /app

COPY package.json .

RUN yarn install --silent

RUN npm install nodemon -g --quiet

COPY . .

EXPOSE 8000

CMD yarn prod