# Agriness Server

Backend server of [AgrinessApp](https://bitbucket.org/mathias5r/agrinessapp/src/master/)


## Requirements

* Docker 
* docker-compose


## Building

```bash
docker-compose build
```

## Running

```bash
docker-compose up -d
```
The agriness server will be running at [http://localhost:8000](http://localhost:8000).

## Testing

```bash
yarn test
```

## Swagger

[http://localhost:8000/swagger](http://localhost:8000/swagger)