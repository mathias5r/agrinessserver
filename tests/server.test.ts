import req from "supertest";
import server from "../src/server";

jest.mock(`../src/services/mongo`,() => ({
  getCollection: jest.fn()
}))

test("[GET] /", async () => {
  const res = await req(server).get("/");
  expect(res.text).toBe("Server Up");
});