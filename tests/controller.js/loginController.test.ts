import supertest from 'supertest';
import server from '../../src/server';
import { HTTP_STATUS } from '../../src/constants'
import { getCollection } from '../../src/services/mongo';
import { mocked } from 'ts-jest/utils'

jest.mock(`../../src/services/mongo`,() => ({
  getCollection: jest.fn()
}))

const mock = mocked(getCollection, true);
const request = supertest(server);

describe(`Test Login Controller`, () => {
  describe(`/login`, () => {
    test(`[POST] Should return not found when user is not found`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => ({}))
        }))
        // act
        const response = await request.post(`/login`);
        // expect
        expect(response.status).toBe(HTTP_STATUS.NOT_FOUND);
    })
    test(`[POST] Should return not authorized when password is wrong`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => ({ hash: 'hash', salt: 'salt'}))
        }))
        // act
        const response = await request.post(`/login`).send({ password: `test`, email: `test`});
        // expect
        expect(response.status).toBe(HTTP_STATUS.NOT_AUTHORIZED);
    })
    test(`[POST] Should return bad request when error`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => Promise.reject(`error`))
        }))
        // act
        const response = await request.post(`/login`).send({ password: `test`, email: `test`});
        // expect
        expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
    })
    test(`[POST] Should return ok when password is correct`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => ({ hash: 'edd5bb5389dfb55beb4d3ac56b7546c17cd464bc4da27aaf95b7dc9cbae8cf1f', salt: 'salt'}))
        }))
        // act
        const response = await request.post(`/login`).send({ password: `test`, email: `test`});
        // expect
        expect(response.status).toBe(HTTP_STATUS.OK);
    })
  })
});