import supertest from 'supertest';
import server from '../../src/server';
import { HTTP_STATUS } from '../../src/constants'
import { getCollection } from '../../src/services/mongo';
import { mocked } from 'ts-jest/utils'

jest.mock(`../../src/services/mongo`,() => ({
  getCollection: jest.fn()
}))

const mock = mocked(getCollection, true);
const request = supertest(server);

describe(`Test Register Controller`, () => {
  describe(`/register`, () => {
    test(`[POST] Should return conflic when user already exist`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => ({ name: 'Fulano' }))
        }))
        // act
        const response = await request.post(`/register`);
        // expect
        expect(response.status).toBe(HTTP_STATUS.CONFLICT);
    })
    test(`[POST] Should return bad request when error`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => Promise.reject(`error`))
        }))
        // act
        const response = await request.post(`/register`).send({ password: `test`, email: `test`});
        // expect
        expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
    })
    test(`[POST] Should return bad request when error`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne: jest.fn().mockImplementation(() => ({})),
          create: jest.fn().mockImplementation(() => true)
        }))
        // act
        const response = await request.post(`/register`).send({ password: `test`, email: `test`});
        // expect
        expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
    })
  })
});