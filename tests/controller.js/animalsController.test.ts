import supertest from 'supertest';
import server from '../../src/server';
import { HTTP_STATUS } from '../../src/constants'
import { getCollection } from '../../src/services/mongo';
import { mocked } from 'ts-jest/utils'

jest.mock(`../../src/services/mongo`,() => ({
  getCollection: jest.fn()
}))

const headers = {
  Authorization : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1hdGhpYXNAdGVzdC5jb20iLCJpYXQiOjE1OTA2MTc4MDZ9.SDeSBnYvg1z7q5vCBDY9dKtA2iDpnCLu_vR9cc4WYBc'
};

const mock = mocked(getCollection, true);
const request = supertest(server);

const mockedAnimal = {
  "_id": "5ec964497a244bd8d308d946",
  "id": "0013c46a-f50f-4072-a165-f14aed3d3750",
  "nome": "SAX648",
  "tipoAnimal": "POULTRY",
  "statusAnimal": 3,
  "localizacao": "Sala 5",
  "dataNascimento": "2017-06-29 02:53",
  "entradaPlantel": "2019-06-16",
  "pesoCompra": 98.934,
  "raca": "ac-7077/m",
  "codigoRastreamento": "742B7DC9863349D2A88A9AE6AC3DDABD",
  "faseProducao": {
      "sigla": "ENG",
      "descricao": "Fase onde os pintinhos são alimentados até o final (abate)"
  },
  "tipoGranja": {
      "sigla": "URE",
      "descricao": "Recria"
  }
}

describe(`Test Animals Controller`, () => {
  describe(`/api/v1/animals`, () => {
    test(`[GET] - Should get not authorized with wrong authorization`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        find: jest.fn().mockImplementation(() => ({ toArray: () => [mockedAnimal]}))
      }))
      // act
      const response = await request.get(`/api/v1/animals`).set({});
      // expect
      expect(response.status).toBe(HTTP_STATUS.NOT_AUTHORIZED);
    })
    test(`[GET] - Should get the list of animals`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          find: jest.fn().mockImplementation(() => ({ toArray: () => [mockedAnimal], limit: () => ({ skip: () => ({ toArray: () => [mockedAnimal]})})}))
        }))
        // act
        const response = await request.get(`/api/v1/animals`).set(headers);
        // expect
        expect(response.status).toBe(HTTP_STATUS.OK);
        expect(response.body).toMatchObject([mockedAnimal])
    })
    test(`[POST] - Should return not authorized with wrong authorization`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        insertOne: jest.fn().mockImplementation(() => Promise.reject(`Error`))
      }))
      // act
      const response = await request.post(`/api/v1/animals`).set({});
      // expect
      expect(response.status).toBe(HTTP_STATUS.NOT_AUTHORIZED);
    })
    test(`[POST] - Should return bad request when error in create`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        insertOne: jest.fn().mockImplementation(() => Promise.reject(`Error`))
      }))
      // act
      const response = await request.post(`/api/v1/animals`).set(headers);
      // expect
      expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
      expect(response.body).toMatchObject({"error": true, "message": "[ERROR]: Error "});
    })
    test(`[POST] - Should return create animal`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        insertOne: jest.fn().mockImplementation(() => Promise.resolve(`0013c46a-f50f-4072-a165-f14aed3d3750`))
      }))
      // act
      const response = await request.post(`/api/v1/animals`).set(headers).send(mockedAnimal);
      // expect
      expect(response.status).toBe(HTTP_STATUS.OK);
      expect(response.body).toMatchObject({"id": "0013c46a-f50f-4072-a165-f14aed3d3750"});
    })
  })
  describe(`/api/v1/animals/:id`, () => {
    test(`[GET] - Should return not authorized with wrong credentials`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        findOne:jest.fn().mockImplementation(() => null)
      }))
      // act
      const response = await request.get(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set({});
      // expect
      expect(response.status).toBe(HTTP_STATUS.NOT_AUTHORIZED);
    })
    test(`[GET] - Should return not found when animal not exits`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        findOne:jest.fn().mockImplementation(() => null)
      }))
      // act
      const response = await request.get(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers);
      // expect
      expect(response.status).toBe(HTTP_STATUS.NOT_FOUND);
      expect(response.text).toBe(`Animal not found`);
    })
    test(`[GET] - Should get animal by id`, async () => {
        // arrange
        mock.mockImplementation(() => Promise.resolve({
          findOne:jest.fn().mockImplementation(() => mockedAnimal)
        }))
        // act
        const response = await request.get(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers);
        // expect
        expect(response.status).toBe(HTTP_STATUS.OK);
        expect(response.body).toMatchObject(mockedAnimal)
    })
    test(`[PATCH] - Should return not authorized with wrong credentials`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        updateOne:jest.fn().mockImplementation(() => Promise.reject(`Error`))
      }))
      // act
      const response = await request.patch(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set({}).send(mockedAnimal);;
      // expect
      expect(response.status).toBe(HTTP_STATUS.NOT_AUTHORIZED);
    })
    test(`[PATCH] - Should return bad request when error in update`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        updateOne:jest.fn().mockImplementation(() => Promise.reject(`Error`))
      }))
      // act
      const response = await request.patch(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers).send(mockedAnimal);;
      // expect
      expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
      expect(response.body).toMatchObject({"error": true, "message": "[ERROR]: Error "})
    })
    test(`[PATCH] - Should update animal`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        updateOne:jest.fn().mockImplementation(() => Promise.resolve(`0013c46a-f50f-4072-a165-f14aed3d3750`))
      }))
      // act
      const response = await request.patch(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers).send(mockedAnimal);;
      // expect
      expect(response.status).toBe(HTTP_STATUS.OK);
    })
    test(`[DELETE] - Should return not authorized with wrong credentials`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        deleteOne:jest.fn().mockImplementation(() => Promise.reject(`Error`))
      }))
      // act
      const response = await request.delete(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers).send(mockedAnimal);;
      // expect
      expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
      expect(response.body).toMatchObject({"error": true, "message": "[ERROR]: Error "})
    })
    test(`[DELETE] - Should return bad request when error in delete`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        deleteOne:jest.fn().mockImplementation(() => Promise.reject(`Error`))
      }))
      // act
      const response = await request.delete(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers).send(mockedAnimal);;
      // expect
      expect(response.status).toBe(HTTP_STATUS.BAD_REQUEST);
      expect(response.body).toMatchObject({"error": true, "message": "[ERROR]: Error "})
    })
    test(`[DELETE] - Should update animal`, async () => {
      // arrange
      mock.mockImplementation(() => Promise.resolve({
        deleteOne:jest.fn().mockImplementation(() => Promise.resolve(`0013c46a-f50f-4072-a165-f14aed3d3750`))
      }))
      // act
      const response = await request.delete(`/api/v1/animals/0013c46a-f50f-4072-a165-f14aed3d3750`).set(headers).send(mockedAnimal);;
      // expect
      expect(response.status).toBe(HTTP_STATUS.OK);
    })
  })
})